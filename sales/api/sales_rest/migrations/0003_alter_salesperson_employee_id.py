# Generated by Django 4.0.3 on 2023-12-19 22:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_alter_customer_address_alter_customer_phone_number_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='employee_id',
            field=models.CharField(max_length=25),
        ),
    ]
