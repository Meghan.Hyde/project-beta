from common.json import ModelEncoder
from .models import Customer, Sale, Salesperson, AutomobileVO


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "import_href"]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "salesperson", "customer", "automobile", "id"]
    encoders = {
        "salesperson": SalespersonEncoder(),
        "automobile": AutomobileVODetailEncoder(),
        "customer": CustomerEncoder(),
    }
    # def get_extra_data(self, o):
    #     return {"automobile": o.automobile.sold}

