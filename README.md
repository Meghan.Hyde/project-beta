# CarCar

Team:

* adedeji adetunji - Service
* Meghan Hyde - Sales

## Design
Microservice Image - see Diagram_PB.png (but this was dropped in the README with a link because I cannot find any other logical way to do this)

![Diagram](Diagram_PB.png)
**Please hold command when clicking on the PNG.

Getting into our project:
Please CD into our project-beta file then follow the steps below in your terminal:
- docker volume create beta-data
- docker-compose build
- docker-compose up

## Inventory microservice
**Special note about the api_urls - if there is a vin(17 number and letters long) or just a number, those are random and can be replaced. As you create data you will create an id or a specific vin. Please what you created (id or VIN) when moving through the data and not the specific VIN or ID numbers provided below.

AUTOMOBILE
api_url for Automobile GET - retrieving a list of automobiles: http://localhost:8100/api/automobiles/
data back from Insomnia could look like this:
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN122366/",
			"id": 3,
			"color": "red",
			"year": 2020,
			"vin": "1C3CC5FB2AN122366",
			"model": {
				"href": "/api/models/3/",
				"id": 3,
				"name": "A3",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/4/",
					"id": 4,
					"name": "Audi"
				}
			},
			"sold": false
		},
    ]
}

api_url for POST - creating an automobile : http://localhost:8100/api/automobiles/
data into Insomnia could look like this:
{
  "color": "white",
  "year": 2021,
  "vin": "1C3CC5FB2AN176600",
  "model_id": 1
}

data from Insomnia would look like this:

{
	"href": "/api/automobiles/1C3CC5FB2AN176600/",
	"id": 6,
	"color": "white",
	"year": 2021,
	"vin": "1C3CC5FB2AN176600",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "x3",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "BMW"
		}
	},
	"sold": false
}

api_url for GET - showing the detail of one automobile: http://localhost:8100/api/automobiles/1C3CC7FB2A6Y20170/
data from Insomnia could look like this:
{
	"href": "/api/automobiles/1C3CC7FB2A6Y20170/",
	"id": 19,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC7FB2A6Y20170",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "tahoe",
		"picture_url": "www.google.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}

api_url for PUT - updating an automobile: http://localhost:8100/api/automobiles/1C3CC5FB2AN166778/
data into Insomnia could look like this:
{
  "sold": true
}
or {
    "sold": true,
    "color": blue,
    "year": 2021
}
**this depends what you need to update

data from Insomnia would look like this:
{
	"href": "/api/automobiles/1C3CC5FB2AN166778/",
	"id": 2,
	"color": "red",
	"year": 2010,
	"vin": "1C3CC5FB2AN166778",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "A3",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Audi"
		}
	},
	"sold": true
}

api_url for DEL - deleting an automobile: http://localhost:8100/api/automobiles/1C3CC7FB2A6Y20170/
data from Insomnia should look like this:
{
	"deleted": true
}

VEHICLES

api_url for GET - retrieving a list of vehicle models: http://localhost:8100/api/models/
data from Insomnia should look like this:
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "x3",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/3/",
				"id": 3,
				"name": "BMW"
			}
		},
    ]
}

api_url for POST - creating a vehicle model: http://localhost:8100/api/models/
data into Insomnia could look like this:
{
  "name": "CRV",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

data from Insomnia could look like this:
{
	"href": "/api/models/4/",
	"id": 4,
	"name": "CRV",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Honda"
	}
}

api_url for GET - retrieving details on one vehicle: http://localhost:8100/api/models/1/
data from Insomnia could look like this:
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}


api_url for PUT - updating a vehicle model: http://localhost:8100/api/models/6/
data into Insomnia could look like this:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

data from Insomnia could look like this:
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}

api_url for DEL - deleting a vehicle: http://localhost:8100/api/models/1/

data from Insomnia could look like this:
{
	"deleted": true
}

MANUFACTURERS

api_url for GET - retrieving a list of manufacturers: http://localhost:8100/api/manufacturers/
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Subaru"
		},
    ]
}

api_url for POST - creating a manufacturer: http://localhost:8100/api/manufacturers/
data into Insomnia could look like this:
{
  "name": "Lexus"
}

data from Insomnia could look like this:
{
	"href": "/api/manufacturers/6/",
	"id": 6,
	"name": "Lexus"
}

api_url for GET - retrieving details on a specific manufacturer: 	http://localhost:8100/api/manufacturers/2/
data from Insomnia:
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "BMW"
}

api_url for PUT - updating a manufacturer: 	http://localhost:8100/api/manufacturers/2/

data into Insomnia would look like this:
{
  "name": "Dodge"
}
data from Insomnia:
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Dodge"
}

api_url for DEL - deleting a manufacturer: http://localhost:8100/api/models/7/
data from Insomnia would look like this:
{
	"deleted": true
}


## Service microservice

For my service microservice I created a Technician model that had the values of first_name, last_name and employee_id. My other model was for a Appointment that held the values of date, time, reason, status, vin, customer, vip, Technician. My third model was for the AutomobileVO witch I created in order to have access to the Automobile model in the root aggregate Inventory microservice. From there, I created encoders for each model so I could reference them in my views as needed.


** special announcement - on the api_urls you will see numbers used, ex. technicians/1/ - that number is an employee ID that is entered each time a piece of data is created. All other data goes by the id created when a piece of data is created. Please use the correct ID based on the data you create not based on my api examples below and that number included..

TECHNICIANS

api_url for GET - list of technicians: http://localhost:8080/api/technicians/
data received from Insomnia:
    {
        "technician": [
            {
                "first_name": "addy",
                "last_name": "tunji",
                "employee_id": 3456,
                "id": 1
            }
        ]
    }

api_url for POST - create a technician: http://localhost:8080/api/technicians/
data input:
    {
        "first_name":"addy",
        "last_name": "tunji",
        "employee_id": 3456
    }
data recieved:
    {
        "first_name": "addy",
        "last_name": "tunji",
        "employee_id": 3456,
        "id": 1
    }

api_url for DELETE - delete a technician: http://localhost:8080/api/technicians/5862/
data recieved:
    {
        "first_name": "adam",
        "last_name": "sandler",
        "employee_id": 5862,
        "id": null
    }

APPOINTMENTS

api_url for GET - list of appointments: http://localhost:8080/api/appointments/
data recieved:
    {
        "appointment": [
            {
                "date": "2023-12-19",
                "time": "04:35:25",
                "reason": "flat tire",
                "status": "submitted",
                "vin": "1C3CC5FB2AN120174",
                "customer": "toon",
                "id": 7,
                "vip": true,
                "technician": {
                    "first_name": "addy",
                    "last_name": "tunji",
                    "employee_id": 3456,
                    "id": 1
                }
            },
            {
                "date": "2024-01-03",
                "time": "19:20:00",
                "reason": "flat tire",
                "status": "Created",
                "vin": "1HGCR2F83DA122381",
                "customer": "jeffery",
                "id": 8,
                "vip": false,
                "technician": {
                    "first_name": "addy",
                    "last_name": "tunji",
                    "employee_id": 3456,
                    "id": 1
                }
            },
            {
                "date": "2024-01-11",
                "time": "17:50:00",
                "reason": "alignment",
                "status": "Finished",
                "vin": "3C6JR6AG8EG337793",
                "customer": "andrew",
                "id": 11,
                "vip": false,
                "technician": {
                    "first_name": "Meghan",
                    "last_name": "Hyde",
                    "employee_id": 5,
                    "id": 3
                }
            },
            {
                "date": "2023-12-27",
                "time": "20:09:00",
                "reason": "dog tore up seats",
                "status": "Canceled",
                "vin": "YV1672MK5B2215789",
                "customer": "meghan",
                "id": 10,
                "vip": false,
                "technician": {
                    "first_name": "adedeji",
                    "last_name": "adetunji",
                    "employee_id": 2102,
                    "id": 2
                }
            },
            {
                "date": "2023-12-19",
                "time": "19:53:00",
                "reason": "a/c maintenance ",
                "status": "Finished",
                "vin": "WBAAW33441EN80275",
                "customer": "wayne",
                "id": 9,
                "vip": false,
                "technician": {
                    "first_name": "adedeji",
                    "last_name": "adetunji",
                    "employee_id": 2102,
                    "id": 2
                }
            },
            {
                "date": "2023-12-12",
                "time": "21:06:00",
                "reason": "dog tore up seats",
                "status": "Created",
                "vin": "3GCPKTE37BG248709",
                "customer": "wayne",
                "id": 14,
                "vip": false,
                "technician": {
                    "first_name": "adam",
                    "last_name": "sandler",
                    "employee_id": 5862,
                    "id": 4
                }
            }
        ]
    }


api_url for POST - create an appointment: http://localhost:8080/api/appointments/
data input:
    {
        "date": "2023-12-19",
        "time": "04:35:25",
        "reason": "flat tire",
        "status":"submitted",
        "vin":"1C3CC5FB2AN120174",
        "customer": "toon",
        "technician": 3456
    }
data recieved:
    {
        "date": "2023-12-19",
        "time": "04:35:25",
        "reason": "flat tire",
        "status": "submitted",
        "vin": "1C3CC5FB2AN120174",
        "customer": "toon",
        "id": 7,
        "technician": {
            "first_name": "addy",
            "last_name": "tunji",
            "employee_id": 3456,
            "id": 1
        }
    }


api_url for DELETE - delete an appointment: http://localhost:8080/api/appointments/9/
data recieved:
    {
        "date": "2023-12-19",
        "time": "19:53:00",
        "reason": "a/c maintenance ",
        "status": "Finished",
        "vin": "WBAAW33441EN80275",
        "customer": "wayne",
        "id": null,
        "vip": false,
        "technician": {
            "first_name": "adedeji",
            "last_name": "adetunji",
            "employee_id": 2102,
            "id": 2
        }
    }


api_url for PUT - finish an appointment: http://localhost:8080/api/appointments/10/finish/
data recieved:
{
	"date": "2023-12-27",
	"time": "20:09:00",
	"reason": "dog tore up seats",
	"status": "Finished",
	"vin": "YV1672MK5B2215789",
	"customer": "meghan",
	"id": 10,
	"vip": false,
	"technician": {
		"first_name": "adedeji",
		"last_name": "adetunji",
		"employee_id": 2102,
		"id": 2
	}
}


api_url for PUT - cancel an appointment: http://localhost:8080/api/appointments/10/cancel/
data recieved:
    {
        "date": "2023-12-27",
        "time": "20:09:00",
        "reason": "dog tore up seats",
        "status": "Canceled",
        "vin": "YV1672MK5B2215789",
        "customer": "meghan",
        "id": 10,
        "vip": false,
        "technician": {
            "first_name": "adedeji",
            "last_name": "adetunji",
            "employee_id": 2102,
            "id": 2
        }
    }


## Sales microservice

For my sales microservice I created a Salesperson model that had the values of first_name, last_name and employee_id. My other model was for a Customer that held the values of first_name, last_name, address and phone_number. My third model was for a Sale that held the value of price and had three foreign keys on it - Customer, Salesperson and AutomobileVO. The Automobile was my fourth model I created in order to have access to the Automobile model in the root aggregate Inventory microservice. From there, I created encoders for each model so I could reference them in my views as needed.


** special announcement - on the api_urls you will see numbers used, ex. sales/1/ - that number is an ID that is created each time a piece of data is created. Please use the correct ID based on the data you create not based on my api examples below and that number included.

SALESPERSON

api_url for GET - list of salespeople: http://localhost:8090/api/salespeople/
data received from Insomnia:

	{
	"salespeople": [
		{
			"first_name": "Betty",
			"last_name": "White",
			"employee_id": "bwhite",
			"id": 1
		},
		{
			"first_name": "Bo",
			"last_name": "Wo",
			"employee_id": "bjo",
			"id": 2
		},
		{
			"first_name": "Will",
			"last_name": "Smith",
			"employee_id": "wsmith",
			"id": 3
		}
	]
}

api_url for POST - creating a salesperson : http://localhost:8090/api/salespeople/
data input to Insomnia could look like this:
{
  "first_name": "Will",
	"last_name": "Smith",
	"employee_id": "wsmith"
}
 data received from Insomnia:
 {
	"first_name": "Will",
	"last_name": "Smith",
	"employee_id": "wsmith",
	"id": 3
}

api_url for DEL - deleting a salesperson: http://localhost:8090/api/salespeople/3/
data received from Insomnia should look like this upon deletion:

{
	"deleted": true
}

CUSTOMER
api_url for GET - retrieving a list of customers: http://localhost:8090/api/customers/
data from Insomnia could look like this:

{
	"customers": [
		{
			"first_name": "Bill",
			"last_name": "Hill",
			"address": "888 Eight lane",
			"phone_number": 2223332222,
			"id": 1
		},
		{
			"first_name": "Jen",
			"last_name": "Aniston",
			"address": "333 third lane",
			"phone_number": 1117779999,
			"id": 2
		},
		{
			"first_name": "Don",
			"last_name": "Jon",
			"address": "119 drive street",
			"phone_number": 9998887777,
			"id": 3
		}
	]
}


api_url for POST - creating a customer: http://localhost:8090/api/customers/
data to Insomnia could look like this:

{
  "first_name": "Jen",
	"last_name": "Aniston",
	"address": "333 third lane",
	"phone_number":1117779999
}

data received from Insomnia would look like this:

{
	"first_name": "Jen",
	"last_name": "Aniston",
	"address": "333 third lane",
	"phone_number": 1117779999,
	"id": 2
}

api_url for DEL - deleting a customer: http://localhost:8090/api/customers/5/
data from Insomnia would look like this:

{
	"deleted": true
}

SALES
api_url for GET - retrieving a sale: http://localhost:8090/api/sales/
data recevied from Insomnia could look like this:

{
	"sales": [
		{
			"price": 10000,
			"salesperson": {
				"first_name": "Bo",
				"last_name": "Wo",
				"employee_id": "bjo",
				"id": 2
			},
			"customer": {
				"first_name": "Jen",
				"last_name": "Aniston",
				"address": "333 third lane",
				"phone_number": 1117779999,
				"id": 2
			},
			"automobile": {
				"vin": "1C3CC5FB2AN177754",
				"sold": true,
				"import_href": "/api/automobiles/1C3CC5FB2AN177754/"
			},
			"id": 10
		},
    ]
}

api_url for POST - creating a sale: http://localhost:8090/api/sales/
data into Insomnia could look like this:

	{
		"price": "34000",
		"customer_id": 1,
		"salesperson_id": 1,
		"automobile": "1C3CC5FB2AN122366"
	}

data from Insomnia could look like this:
{
	"price": "34000",
	"salesperson": {
		"first_name": "Betty",
		"last_name": "White",
		"employee_id": "bwhite",
		"id": 1
	},
	"customer": {
		"first_name": "Bill",
		"last_name": "Hill",
		"address": "888 Eight lane",
		"phone_number": 2223332222,
		"id": 1
	},
	"automobile": {
		"vin": "1C3CC5FB2AN122366",
		"sold": true,
		"import_href": "/api/automobiles/1C3CC5FB2AN122366/"
	},
	"id": 11
}

api_url for DEL - deleting a sale: http://localhost:8090/api/sales/1/
data received from Insomnia would look like this:

{
	"deleted": true
}
