import React, {useEffect, useState} from "react";


function AutoList() {
    const [autos, setAutos] = useState([]);
    const [sales, setSales] = useState([]);

    const fetchSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/'
        const response = await fetch(salesUrl);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    const fetchData = async () => {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(autoUrl);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }


    const changeToSold = async () => (auto => {
        const soldVins = sales["automobile"]["vins"]
        if (soldVins === undefined){
            return null
        } else {
            soldVins.map(vins => {
                if (auto.vin === vins.vin) {
                    auto.sold = true;
                    auto.save()
                }
            })
        }
    })


    useEffect(() => {
        fetchData();
        fetchSales();
        changeToSold(autos);
    }, [])


    return (
        <>
        <h1>Automobiles</h1>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Year</th>
                    <th>Color</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody className="col">
                {autos.map(auto => {
                    return (
                        <tr key={auto.href}>
                            <td>{auto.vin}</td>
                            <td>{auto.year}</td>
                            <td>{auto.color}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                            <td>{auto.sold ? "Yes" : "No" }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}


export default AutoList;
