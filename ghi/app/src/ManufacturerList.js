import { useEffect, useState } from 'react';


function ManufacturerList() {

    const [manufacturers, setManufacturers] = useState([]);

    async function getManufacturers() {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturerUrl)
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        };

    }

    useEffect(() => {
        getManufacturers();
    }, [] );

    return (

        <table className="table table-striped table-hover">
          <thead className='thead table-success'>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
          {manufacturers.map(manufacturer => {
            return (
            <tr key={ manufacturer.id }>
              <td>{ manufacturer.name }</td>
            </tr>

          );
        })}
          </tbody>
        </table>
        );

}

export default ManufacturerList;
