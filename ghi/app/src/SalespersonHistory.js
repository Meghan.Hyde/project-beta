import React, { useState, useEffect } from "react";


function SalespersonHistory() {

    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [filteredSales, setFilteredSales] = useState([]);
    const [sales, setSales] = useState([]);


    const handleSalespersonChange = (event) => {
        setSalesperson(event.target.value);
    }

    const handleSalesChange = (event) => {
        setSales(event.target.value);
    }


    async function fetchSalespeople() {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    };


    async function fetchSales() {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl)
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        };

    }


    useEffect(() => {
        fetchSalespeople();
        fetchSales();
    }, [] );


    const filteredSalesBySalesperson = () => {
        const filteredData = sales.filter(sale => sale.salesperson.id.toString() === salesperson);
        setFilteredSales(filteredData)

    }

    useEffect(() => {
        filteredSalesBySalesperson();
    }, [salesperson, sales])


    return (

        <div>
        <h1>Salesperson Sale History</h1>

            <div className="mb-3">
            <select onChange={handleSalespersonChange}
            required name="salesperson"
            id="salesperson"
            value={salesperson}
            className="form-select">
                <option value="">Select Salesperson</option>
                  {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                  })}
                </select>
              </div>


    <table className="table table-striped table-hover">
          <thead className='thead table-success'>
            <tr>
              <th>Salesperson Name</th>
              <th>Salesperson ID</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
          {filteredSales.map(sale => {
            return (
            <tr key={sale.id}>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name }</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
            </tr>

          );
        })}
          </tbody>
        </table>


    </div>
  );



}

export default SalespersonHistory;
