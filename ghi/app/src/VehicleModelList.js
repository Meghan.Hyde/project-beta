import React, {useEffect, useState} from "react";


function ModelList() {
    const [models, setModel] = useState([]);
    const fetchData = async () => {
        const modelUrl = 'http://localhost:8100/api/models/';
        const response = await fetch(modelUrl);
        if (response.ok) {
            const data = await response.json();
            setModel(data.models);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])
    return (
        <>
        <h1>Models</h1>
        <div className="col">
          {models.map(model => {
            return (
              <div key={model.href} className="card mb-3 shadow" style={{width:'18rem', display:'-webkit-inline-flex'}}>
                <img src={model.picture_url} className="card-img-top" />
                <div className="card-body">
                  <h2 className="card-title">Model Name: {model.name}</h2>
                  <h3 className="card-title">Manufacturer Name: {model.manufacturer.name}</h3>
                </div>
              </div>
            );
          })}
        </div>
        </>
    )
}


export default ModelList;
