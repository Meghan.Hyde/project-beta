import { useEffect, useState } from 'react';


function SalesList(props) {

    const [sales, setSales] = useState([]);

    async function getSales() {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl)
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        };

    }

    useEffect(() => {
        getSales();
    }, [] );

    return (

        <table className="table table-striped table-hover">
          <thead className='thead table-success'>
            <tr>
              <th>Salesperson Name</th>
              <th>Salesperson ID</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
          {sales.map(sale => {
            const customerName = `${sale.customer.first_name} ${sale.customer.last_name}`;
            const salespersonName = `${sale.salesperson.first_name} ${sale.salesperson.last_name}`;
            return (
            <tr key={sale.id}>
              <td>{ customerName }</td>
              <td>{ sale.salesperson.employee_id}</td>
              <td>{ salespersonName }</td>
              <td>{ sale.automobile.vin}</td>
              <td>{ sale.price}</td>
            </tr>

          );
        })}
          </tbody>
        </table>
        );

}
export default SalesList;
