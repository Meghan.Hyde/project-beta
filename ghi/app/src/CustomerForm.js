import React, { useState } from 'react';

function CustomerForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    }

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    }

    const handleAddressChange = (event) => {
        setAddress(event.target.value);
    }

    const handlePhoneNumberChange = (event) => {
        setPhoneNumber(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer)

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');

        }

    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Customer</h1>
            <form onSubmit={handleSubmit}
            id="create-customer-form">

                <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange}
                placeholder="first_name"
                required type="text"
                name="first_name"
                id="first_name"
                value={firstName}
                className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange}
                placeholder="last_name"
                required type="text"
                name="last_name"
                id="last_name"
                value={lastName}
                className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleAddressChange}
                placeholder="Address"
                required type="text"
                name="address"
                id="address"
                value={address}
                className="form-control" />
                <label htmlFor="address">Address</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handlePhoneNumberChange}
                placeholder="xxx xxx xxxx"
                required type="text"
                name="phone_number"
                id="phone_number"
                value={phoneNumber}
                className="form-control" />
                <label htmlFor="phone_number">Phone Number (9 digits) </label>
              </div>


              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default CustomerForm;
