import React, { useEffect, useState } from 'react';


function NewSaleForm() {

    const [automobile, setAutomobile] = useState('');
    const [autos, setAutos] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    const [filteredVins, setFilteredVins] = useState([]);


    const handleAutomobileChange = (event) => {
        setAutomobile(event.target.value);
    }

    const handleSalespersonChange = (event) => {
        setSalesperson(event.target.value);
    }

    const handleCustomerChange = (event) => {
        setCustomer(event.target.value);
    }

    const handlePriceChange = (event) => {
        setPrice(event.target.value);
    }


    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};

      data.automobile = automobile;
      data.salesperson_id = salesperson;
      data.customer_id = customer;
      data.price = price;

      const salesUrl = 'http://localhost:8090/api/sales/';
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        }
      };


      const response = await fetch(salesUrl, fetchConfig);
      if (response.ok) {
        const newSale = await response.json();
        console.log(newSale)

        setAutomobile('');
        setSalesperson('');
        setCustomer('');
        setPrice('');

      }
    };

    async function fetchAutomobiles() {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(automobileUrl);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
        else {
          console.error('An error occured while fetching data for automobiles')
        }

    };

    async function fetchSalespeople() {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
        else {
          console.error('An error occured while fetching data for salespeople')
        }

    };

    async function fetchCustomers() {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
        else {
          console.error('An error occured while fetching data for customers')
        }

    };

    useEffect(() => {
        fetchAutomobiles();
        fetchSalespeople();
        fetchCustomers();
    }, [] );



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Sale</h1>
            <form onSubmit={handleSubmit}
            id="create-newsale-form">

                <div className="mb-3">
                <select onChange={handleAutomobileChange}
                required name="automobile"
                id="automobile"
                value={automobile}
                className="form-select">
                  <option value="">Choose VIN</option>
                  {autos.filter(automobile => automobile.sold === false)
                    .map(automobile => {
                    return (
                        <option key={automobile.vin} value={automobile.vin}>
                            {automobile.vin}
                        </option>
                    );
                  })}
                </select>
              </div>

              <div className="mb-3">
                <select onChange={handleSalespersonChange}
                required name="salesperson"
                id="salesperson"
                value={salesperson}
                className="form-select">
                  <option value="">Select Salesperson</option>
                  {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                  })}
                </select>
              </div>

              <div className='mb-3'>
                <select onChange={handleCustomerChange}
                require name ="customer"
                id="customer"
                value={customer}
                className='form-select'>
                  <option value="">Select Customer</option>
                  {customers.map(customer => {
                    return(
                      <option key={customer.id} value={customer.id}>
                        {customer.first_name} {customer.last_name}
                      </option>
                    )
                  })}
                </select>

              </div>

              <div className="form-floating mb-3">
                <input onChange={handlePriceChange}
                placeholder="Price"
                required type="text"
                name="price"
                id="price"
                value={price}
                className="form-control" />
                <label htmlFor="price">Price</label>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default NewSaleForm;
