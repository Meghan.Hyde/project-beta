import React, { useState, useEffect } from 'react';

function VehicleForm() {

    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturerId, setManufacturerId] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    const handleNameChange = (event) => {
        setName(event.target.value);
    }

    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }

    const handleManufacturerIdChange = (event) => {
        setManufacturerId(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturerId;

    const vehicleUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    };

    const response = await fetch(vehicleUrl, fetchConfig);
    console.log('response:', response);
    if (response.ok) {
        const newVehicle = await response.json();
        console.log(newVehicle)

        setName('');
        setPictureUrl('');
        setManufacturerId('');
    }

    };

    async function fetchManufacturers() {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturerUrl);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);

        }
        else {
            console.error('An error occured while fetching manufacturer data')
        }

    };

    useEffect(() => {
        fetchManufacturers();
    }, [] );



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Vehicle</h1>
            <form onSubmit={handleSubmit}
            id="create-vehicle-form">

              <div className="form-floating mb-3">
                <input onChange={handleNameChange}
                placeholder="Name"
                required type="text"
                name="name"
                id="name"
                value={name}
                className="form-control" />
                <label htmlFor="name">Model Name</label>
              </div>

              <div className="mb-3">
                <label htmlFor="picture_url">Picture Url</label>
                <input onChange={handlePictureUrlChange}
                name="picture_url"
                id="picture_url"
                value={pictureUrl}
                className="form-control"></input>
              </div>

              <div className="mb-3">
                <select onChange={handleManufacturerIdChange}
                required name="manufacturer"
                id="manufacturer"
                value={manufacturerId}
                className="form-select">
                  <option value="">Choose Manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default VehicleForm;
